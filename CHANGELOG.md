
# Changelog for Invite Members portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.7.1] - 2020-12-11

 - Fix for issue on test #23646

## [v2.7.0] - 2020-12-11

 - Feature #19463, gestione Inviti include Keycloak per quanto riguarda la creazione dello user account

 - Ported to git

 - Ported to GWT 2.8.2

## [v2.6.0] - 2019-04-08

Feature #16458, reconsider the message

Feature #16461, prevent the system from inviting people having an already registered email in VRE

## [v2.5.0] - 2018-06-25

Feature #11900, Revise email templates to not be intercepted by MailScanner

## [v2.4.0] - 2017-11-29

Ported to GWT 2.8.1

## [v2.3.0] - 2017-05-12

Invite emails are now sent via Email Templates Library

## [v1.2.0] - 2015-07-03

Feature #330, Integrated invites handling via common widgete 

Fix bug #241 When an invite is performed more than once the button invite disappears and only Close is available

## [v1.0.0] - 2015-04-30

First release 
